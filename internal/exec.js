const getFile = require('./getFile');

module.exports = (conn, debug) => async (sql, parameters) => {

  // Execute
  const obj = await Promise.all([conn, getFile(sql, debug)]);

  let request = obj[0].request();

  // Add input parameters
  Object.keys(parameters).forEach(param => {
    debug(`Command parameter '${param}' is added`);
    request = request.input(param, parameters[param]);
  });

  // Execute
  return request.query(obj[1]);
};
