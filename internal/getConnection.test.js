const { expect } = require('chai');
const getConnection = require('./getConnection');

describe('dcDatabase: Connection', () => {
  it('Can connect to DB', async() => {
    const pool = await getConnection('mssql://DCUser:sdfg7654erg%25ERYTRfrar@seaexc-mlkqn3elb3equ.database.secure.windows.net/DentalChartDb?encrypt=true');
    expect(pool).not.to.be.null;
  });

  it('Can read ordinacije for test', async() => {
    const pool = await getConnection('mssql://DCUser:sdfg7654erg%25ERYTRfrar@seaexc-mlkqn3elb3equ.database.secure.windows.net/DentalChartDb?encrypt=true');
    const rows = await pool.query('SELECT * FROM ordinacije;');
    expect(rows.recordset.length).to.be.equal(13);
  });
});