const fs = require('fs');
const path = require('path');

const cache = {};

module.exports = (file, debug) => new Promise((resolve, reject) => {
  debug(`Retrieving SQL statement from a file ${file}`);
  const filePath = path.resolve(file);
  if(!cache[filePath])
    fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) {
        debug(`File ${file} with SQL statement is not available`);
        reject(err);
      }
      else {
        cache[filePath] = data;
        resolve(cache[filePath])
      }
    });
  else
    resolve(cache[filePath]);
});