const sql = require('mssql');
const connStringParser = require('./connectionStringParser');

let conn = null;

module.exports = async (connectionString, debug) => {

  // If connection is not established, do it
  if(!conn) {
    debug('Database is not initialized, trying...');
    debug(connStringParser.resolve(connectionString));
    try {
      conn = await new sql.ConnectionPool(connStringParser.resolve(connectionString)).connect();
      debug('Database connection pool is initialized');
    }
    catch(ex) {
      debug('Database connection pool initialization has failed');
      console.error(ex);
      conn = null;
      throw ex;
    }
  }

  // Return established connection pool
  return conn;
};
