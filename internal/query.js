const getFile = require('./getFile');

module.exports = (conn, cache, debug) => async (sql, parameters, cacheOptions) => {

  if(cacheOptions && cacheOptions.key) {
    debug('Query cache is trying to fetch')
    const result = await cache.get(cacheOptions.key);
    if(result) {
      debug(`Data is found in cache, ${cacheOptions.key}, returning...`);
      return result;
    }
    else {
      debug(`No cache for this item, ${cacheOptions.key}`);
    }
  }

  // Execute
  const obj = await Promise.all([conn, getFile(sql, debug)]);
  let request = obj[0].request();

  // Add input parameters
  Object.keys(parameters).forEach(param => {
    debug(`Query parameter '${param}' is added`);
    request = request.input(param, parameters[param]);
  });

  // Execute query
  const result = await request.query(obj[1]);

  if(cacheOptions && cacheOptions.key) {
    await cache.set(cacheOptions.key, result.recordsets && result.recordsets.length > 1 || result.recordset, cacheOptions.ttl);
    debug(`Data for ${cacheOptions.key} is saved to cache for ${cacheOptions.ttl} seconds`);
  }
  return result.recordsets && result.recordsets.length > 1 || result.recordset;

};
