const sql = require('mssql');
const getConnection = require('./internal/getConnection');
const query = require('./internal/query');
const exec = require('./internal/exec');
const NodeCache = require('node-cache');
const debug = require('debug')('dc-database');
debug('Initializing module');

module.exports =  (connectionString) => {
  const conn = getConnection(connectionString, debug);
  return {
    query: query(conn, new NodeCache(), debug),
    exec: exec(conn, debug),
    sql
  };
};
